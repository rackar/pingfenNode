var base64id = require("base64id");
socket_io_like=(socket,cb)=> {
  socket.id = base64id.generateId();
  socket._broadcast=(type,data,group)=>{
    let groups=[]
    if(typeof group==="string"){
      socket.clients.forEach((client)=>{
        if(client.group==group){
          groups.push(client)
        }
      })
    }
    else{
      groups=socket.clients
    }
    groups.forEach((_socket)=>{
      _socket._emit(type,data)
    })
  }
  socket.on("message", value => {
    try{
      let JSONdata=JSON.parse(value)
      socket.emit(JSONdata.type,JSONdata.data)
    }
    catch (error){
      console.log('socket send error!',error)
      socket._emit(null,error.message)
    }
  });
  socket._emit=(fun,value)=>{
    let data={
      type:fun,
      data:value
    }
    let _text=JSON.stringify(data)
    socket.send(_text);
  }
  cb(socket)
  socket._emit('onopen',{
    status:true,
    data:socket.id,
    message:'conneted'
  })
  console.log(socket.id)
}
module.exports =socket_io_like;
  