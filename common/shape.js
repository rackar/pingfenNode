
var fs = require("fs");
var iconv = require('iconv-lite');//如果属性中有汉字的需要这个转一下
class geojson 
{
    constructor() {
        this.features=[];
    }
    AddFeature(properties,geometry){
        let feature=new Feature();
        feature.AddPropertie(properties)
        feature.AddGeometry(geometry)
        this.features.push(feature);
    }
    GetFeature(_FeatureCollection=false){
        if(_FeatureCollection==true){
            return {
                type:'FeatureCollection',
                features:this.features
            }
        }
        else{
            if(this.features.length==1){
                return this.features[0]
            }
            else if(this.features.length==0){
                return {
                    type:'FeatureCollection',
                    features:[]
                }
            }
            else {
                return {
                    type:'FeatureCollection',
                    features:this.features
                }
            }
        }
        
    }
}
class Feature 
{
    constructor() {
        this.type='Feature'
        this.properties={};
        this.geometry={};
    }
    AddPropertie(properties){
        this.properties=properties;
    }
    AddGeometry(geometry){
        let type=geometry.type;
        if(type=='point'){
            this.geometry.coordinates=[geometry.x,geometry.y]
            this.geometry.type="Point"
        }
        else if(type=='MultiPoint' && Array.isArray(geometry))
        {
            this.geometry.coordinates=[];
            for (let key of geometry) {
                this.geometry.push([key.x,key.y])
            }
            this.geometry.type="MultiPoint"
        }
        else if(type=='polyline'){
            this.geometry.coordinates=geometry.rings
            this.geometry.type="LineString"
        }
        else if(type=='MultiLineString' && Array.isArray(geometry)){
            this.geometry=[];
            for (let key of geometry) {
                this.geometry.push([key.paths])
            }
        }
        else if(type=='polygon'){
            this.geometry.coordinates=geometry.rings
            this.geometry.type="Polygon"
        }
        else if(type=='MultiPlygon' && Array.isArray(geometry)){
            this.geometry=[];
            for (let key of geometry) {
                this.geometry.push([key.rings])
            }
        }
    }
}
var ShpType = {
    SHAPE_UNKNOWN : -1,//Unknow Shape Type (for internal use) 
    SHAPE_NULL : 0,//ESRI Shapefile Null Shape shape type.
    SHAPE_POINT : 1,//ESRI Shapefile Point Shape shape type.
    SHAPE_POLYLINE : 3,//ESRI Shapefile PolyLine Shape shape type.
    SHAPE_POLYGON : 5,//ESRI Shapefile Polygon Shape shape type.
    SHAPE_MULTIPOINT : 8,//ESRI Shapefile Multipoint Shape shape type (currently unsupported).
    SHAPE_POINTZ : 11,//ESRI Shapefile PointZ Shape shape type.
    SHAPE_POLYLINEZ : 13,//ESRI Shapefile PolylineZ Shape shape type(currently unsupported).
    SHAPE_POLYGONZ : 15,//ESRI Shapefile PolygonZ Shape shape type (currently unsupported).
    SHAPE_MULTIPOINTZ : 18,//ESRI Shapefile MultipointZ Shape shape type (currently unsupported).
    SHAPE_POINTM : 21,//ESRI Shapefile PointM Shape shape type
    SHAPE_POLYLINEM : 23,//ESRI Shapefile PolyLineM Shape shape type (currently unsupported).
    SHAPE_POLYGONM : 25,// ESRI Shapefile PolygonM Shape shape type (currently unsupported).
    SHAPE_MULTIPOINTM : 28,//ESRI Shapefile MultiPointM Shape shape type (currently unsupported).
    SHAPE_MULTIPATCH : 31//ESRI Shapefile MultiPatch Shape shape type (currently unsupported).
};
var ShapeFieldType = {
    SHAPE_B : 66,
    SHAPE_C : 67,
    SHAPE_D : 68,
    SHAPE_F : 70,
    SHAPE_G : 71,
    SHAPE_N : 78,
    SHAPE_I : 73,
    SHAPE_L : 76,
    SHAPE_M : 77,
    SHAPE_T : 84,
    SHAPE_P : 80,
    SHAPE_Y : 89 
}
var LDID = {
    UTF_16 : 79,
    UTF_8 : 87,
    ASNI : 77,//for gb2312
}
class Shape 
{
    constructor() {
        this.reset();
    }
    reset(){
        this.shp="";
        this.dbf="";
        this.prj="";
        this.Shpheader_Offset=0
        this.Dbfheader_Offset=0
        //shape相关的变量
        this.shapeType=null;
        this.shapeType_string=null;
        this.shp_recorde_values=[];//解析后的shp值
        //dbf相关的变量
        this.dbf_recorde_count=null;
        this.dbf_fileds=null;
        this.LDID=null;//Language driver ID. 29  编码leixing
        this.dbf_recorde_values=[];//解析后的dbf值
        this.shp_file_buffer=null
        this.dbf_file_buffer=null
    }
    async Read(options){
        let _options=Object.assign({},options);
        this.shp=_options.shp
        this.dbf=_options.dbf;
        this.prj=_options.prj;
        if(Array.isArray(this.shp) || fs.existsSync(this.shp)){
            if(Array.isArray(this.shp)){
                this.shp_file_buffer=this.Read_file_by_Array(this.shp)
            }
            else{

                this.shp_file_buffer=this.Read_file_buffer(this.shp)
            }
            if(this.shp_file_buffer==null ||this.shp_file_buffer.length<=100){
                return {
                    status:false,
                    message:"读取shape文件失败"
                }
            }
            if(!this.Read_Shp_header_By_buffer()){
                return {
                    status:false,
                    message:"读取shape文件头失败"
                }
            }
            if(!this.Read_shp_Record_By_buffer()){
                return {
                    status:false,
                    message:"读取shape文件体失败 或不支持该shape geom格式"
                }
            }
        }
        if(fs.existsSync(this.dbf) ){
            this.dbf_file_buffer=this.Read_file_buffer(this.dbf)
            if(this.dbf_file_buffer==null ){
                return {
                    status:false,
                    message:"读取dbf文件失败"
                }
            }
            if(!this.Read_dbf_header_By_buffer()){
                return {
                    status:false,
                    message:"读取dbf文件头失败"
                }
            }
            if(!this.Read_dbf_Record_By_buffer()){
                return {
                    status:false,
                    message:"读取dbf文件体失败"
                }
            }
        }
        let _geojson=new geojson();
        if(this.shp_recorde_values.length==this.dbf_recorde_values.length){
            for (let index = 0; index < this.shp_recorde_values.length; index++) {
                _geojson.AddFeature(this.dbf_recorde_values[index],this.shp_recorde_values[index])
            }
        }
        else if(this.shp_recorde_values.length>this.dbf_recorde_values.length){
            for (let index = 0; index < this.shp_recorde_values.length; index++) {
                _geojson.AddFeature({},this.shp_recorde_values[index])
            }
        }
        else if(this.shp_recorde_values.length<this.dbf_recorde_values.length){
            for (let index = 0; index < this.dbf_recorde_values.length; index++) {
                _geojson.AddFeature(this.dbf_recorde_values[index],{},)
            }
        }
        return {
            status:true,
            result:_geojson.GetFeature(true)
        }
    }
    callbackFun(cb,error,data){
        if(typeof cb==="function"){
            cb(error,data)
        }
    }
    Read_file_buffer(_file){
        try{
            let _file_open=fs.openSync(_file,'r');
            let _file_open_stat=fs.statSync(_file);
            let _file_buffer=Buffer.alloc(_file_open_stat.size);
            fs.readSync(_file_open, _file_buffer, 0, _file_open_stat.size, 0); //读入文件数据到buffer
            fs.closeSync(_file_open);
            return _file_buffer;
        }
        catch(e){
            return null
        }
    }
    Read_file_by_Array(_array){
        try{
            let _file_buffer=Buffer.from(_array);//读入文件数据到buffer
            return _file_buffer;
        }
        catch(e){
            return null
        }
    }
    Read_Shp_header_By_buffer(){
        try{
            let i_00_03=this.shp_file_buffer.readUInt32BE(this.Shpheader_Offset);this.Shpheader_Offset+=4;
            let i_04_07 =this.shp_file_buffer.readUInt32BE(this.Shpheader_Offset);this.Shpheader_Offset+=4;
            let i_08_11 =this.shp_file_buffer.readUInt32BE(this.Shpheader_Offset);this.Shpheader_Offset+=4;
            let i_12_15 =this.shp_file_buffer.readUInt32BE(this.Shpheader_Offset);this.Shpheader_Offset+=4;
            let i_16_19 =this.shp_file_buffer.readUInt32BE(this.Shpheader_Offset);this.Shpheader_Offset+=4;
            let i_20_23 =this.shp_file_buffer.readUInt32BE(this.Shpheader_Offset);this.Shpheader_Offset+=4;
            let i_24_27 =this.shp_file_buffer.readUInt32BE(this.Shpheader_Offset);this.Shpheader_Offset+=4;//文件的实际长度
            let i_28_31 =this.shp_file_buffer.readUInt32LE(this.Shpheader_Offset);this.Shpheader_Offset+=4;//版本号
            let shapeType_32_35=this.shp_file_buffer.readUInt32LE(this.Shpheader_Offset);this.Shpheader_Offset+=4;this.shapeType=shapeType_32_35
            let d_36_43=this.shp_file_buffer.readDoubleLE(this.Shpheader_Offset);this.Shpheader_Offset+=8;//Xmin
            let d_44_51=this.shp_file_buffer.readDoubleLE(this.Shpheader_Offset);this.Shpheader_Offset+=8;//Ymin
            let d_52_59=this.shp_file_buffer.readDoubleLE(this.Shpheader_Offset);this.Shpheader_Offset+=8;//Xmax
            let d_60_67=this.shp_file_buffer.readDoubleLE(this.Shpheader_Offset);this.Shpheader_Offset+=8;//Ymax
            let d_68_75=this.shp_file_buffer.readDoubleLE(this.Shpheader_Offset);this.Shpheader_Offset+=8;//Zmin
            let d_76_83=this.shp_file_buffer.readDoubleLE(this.Shpheader_Offset);this.Shpheader_Offset+=8;//Zmax
            let d_84_91=this.shp_file_buffer.readDoubleLE(this.Shpheader_Offset);this.Shpheader_Offset+=8;//Mmin
            let d_92_99=this.shp_file_buffer.readDoubleLE(this.Shpheader_Offset);this.Shpheader_Offset+=8;//Mmax
            return true
        }
        catch(e){
            return false
        }
        //解析SHAPE头文件完毕！
    }
    Read_shp_Record_By_buffer(){
        let e=true;
        switch(this.shapeType) {
            case ShpType.SHAPE_POINT:
                e=this.Read_shpRecord_By_buffer_POINT();
                break;
            case ShpType.SHAPE_POLYGON:
                e=this.Read_shpRecord_By_buffer_POLYGON_POLYLINE("polygon");
                break;
            case ShpType.SHAPE_POLYLINE:
                e=this.Read_shpRecord_By_buffer_POLYGON_POLYLINE("polyline");
                break;
            default:        
                e=false
                break;
        }
        return e;
    }
    Read_shpRecord_By_buffer_POINT(){
        try{
            let shps=[]
            while (this.Shpheader_Offset<this.shp_file_buffer.length)
            {
                let thisp={
                    type:"point"
                };
                let index=this.shp_file_buffer.readUInt32BE(this.Shpheader_Offset);this.Shpheader_Offset+=4;//记录号 记录号都是从1开始的,下面1行代码是反转位序
                let leng=this.shp_file_buffer.readUInt32BE(this.Shpheader_Offset);this.Shpheader_Offset+=4; //坐标记录长度,是不是为坐标的小数点位数？
                let i_ShapeType =this.shp_file_buffer.readInt32LE(this.Shpheader_Offset);this.Shpheader_Offset+=4;//坐标表示的类型
                thisp.x =this.shp_file_buffer.readDoubleLE(this.Shpheader_Offset);this.Shpheader_Offset+=8;//当前要素的X
                thisp.y =this.shp_file_buffer.readDoubleLE(this.Shpheader_Offset);this.Shpheader_Offset+=8;//当前要素的Y
                shps.push(thisp);
            }
            this.shp_recorde_values=shps
            return true;
        }
        catch(e){
            return false
        }
    }
    Read_shpRecord_By_buffer_POLYGON_POLYLINE(type=""){
        try{
            let shps=[]
            while (this.Shpheader_Offset<this.shp_file_buffer.length)
            {
                let thisp={
                    type
                };
                //记录的头
                let index=this.shp_file_buffer.readUInt32BE(this.Shpheader_Offset);this.Shpheader_Offset+=4;
                let leng=this.shp_file_buffer.readUInt32BE(this.Shpheader_Offset);this.Shpheader_Offset+=4;//坐标记录长度,是不是为坐标的小数点位数？
                let i_ShapeType =this.shp_file_buffer.readInt32LE(this.Shpheader_Offset);this.Shpheader_Offset+=4;
                let Xmin =this.shp_file_buffer.readDoubleLE(this.Shpheader_Offset);this.Shpheader_Offset+=8;
                let Ymin =this.shp_file_buffer.readDoubleLE(this.Shpheader_Offset);this.Shpheader_Offset+=8;
                let Xmax =this.shp_file_buffer.readDoubleLE(this.Shpheader_Offset);this.Shpheader_Offset+=8;
                let Ymax=this.shp_file_buffer.readDoubleLE(this.Shpheader_Offset);this.Shpheader_Offset+=8;
                let NumParts=this.shp_file_buffer.readUInt32LE(this.Shpheader_Offset);this.Shpheader_Offset+=4;
                let NumPoints=this.shp_file_buffer.readUInt32LE(this.Shpheader_Offset);this.Shpheader_Offset+=4;
                let Parts=[];
                //Parts数组记录了每个子环的坐标在Points数组中的起始位置
                for (var i = 0; i < NumParts; i++)
                {
                    let offset=this.shp_file_buffer.readInt32LE(this.Shpheader_Offset);this.Shpheader_Offset+=4;
                    Parts.push(offset);  //每个子环的坐标在Points数组中的起始位置
                }
                //Points数组 记录了所有的坐标信息
                let Points = [];
                for (let i = 0; i < NumPoints; i++)
                {
                    let shppoint={};
                    shppoint.x = this.shp_file_buffer.readDoubleLE(this.Shpheader_Offset);this.Shpheader_Offset+=8;
                    shppoint.y =this.shp_file_buffer.readDoubleLE(this.Shpheader_Offset);this.Shpheader_Offset+=8;
                    Points.push(shppoint);
                }
                let rings=[];
                for (let j = 0; j < NumParts; j++)
                {
                    let i_start =Parts[j];
                    let one_polygon=[];
                    let i_end =(j + 1 < NumParts)?Parts[j + 1]:Points.length;
                    for (let ii = i_start; ii < i_end; ii++)
                    {
                        var shppoint = Points[ii];
                        var one_polygon_one_point = [];
                        //如果存在的是平面坐标需要修改成为经纬度坐标  以后改
                        one_polygon_one_point.push(shppoint.x);
                        one_polygon_one_point.push(shppoint.y);
                        one_polygon.push(one_polygon_one_point);
                    }
                    rings.push(one_polygon);
                }
                thisp.rings=rings;
                shps.push(thisp);
            }
            this.shp_recorde_values=shps
            return true;
        }
        catch(e){
            return false
        }
    }
    Read_dbf_header_By_buffer(){ //读取dbf头文件 不定长字节
        try{
            var i_0 =this.dbf_file_buffer.readInt8(this.Dbfheader_Offset);this.Dbfheader_Offset+=1;//表示当前的版本信息
            this.Dbfheader_Offset+=3;//表示最近的更新日期，按照YYMMDD格式。
            var record_i_4_7 =this.dbf_file_buffer.readUInt32LE(this.Dbfheader_Offset);this.Dbfheader_Offset+=4;//记录条数
            this.dbf_recorde_count=record_i_4_7
            var header_count_8_9=this.dbf_file_buffer.readInt16LE(this.Dbfheader_Offset);this.Dbfheader_Offset+=2;//文件头中的字节数
            var i_10_11 =this.dbf_file_buffer.readInt16LE(this.Dbfheader_Offset);this.Dbfheader_Offset+=2;//一条记录中的字节长度
            this.Dbfheader_Offset+=2;//2个字节	保留字节，用于以后添加新的说明性信息时使用，这里用0来填写。
            this.Dbfheader_Offset+=1;//表示未完成的操作。
            this.Dbfheader_Offset+=1;//dBASE IV编密码标记。
            this.Dbfheader_Offset+=12;//保留字节，用于多用户处理时使用
            this.Dbfheader_Offset+=1;//DBF文件的MDX标识。在创建一个DBF 表时 ，如果使用了MDX 格式的索引文件，那么 DBF 表的表头中的这个字节就自动被设置了一个标志，当你下次试图重新打开这个DBF表的时候，数据引擎会自动识别这个标志，如果此标志为真，则数据引擎将试图打开相应的MDX 文件
            var LDID=this.dbf_file_buffer.readInt8(this.Dbfheader_Offset);this.Dbfheader_Offset+=1;//Language driver ID. 29  编码字节
            this.LDID=LDID
            this.Dbfheader_Offset+=2;//保留字节，用于以后添加新的说明性信息时使用，这里用0来填写。
            var d_sxnum=(header_count_8_9 - 1 - 32) / 32;//计算出属性列的个数
            var fileds=[];
            for (let i = 0; i < d_sxnum;i ++ )
            {
                let Onefiled_Name_Buffer=Buffer.alloc(11);
                this.dbf_file_buffer.copy(Onefiled_Name_Buffer, 0, this.Dbfheader_Offset, this.Dbfheader_Offset+11);this.Dbfheader_Offset+=11;//记录项名称，是ASCII码值。
                let name=this.ByteToStr(this.trim(Onefiled_Name_Buffer));
                let Onefiled_Type_Buffer=Buffer.alloc(1);
                this.dbf_file_buffer.copy(Onefiled_Type_Buffer, 0, this.Dbfheader_Offset, this.Dbfheader_Offset+1);this.Dbfheader_Offset+=1;//记录项的数据类型，是ASCII码值(B、C、D、G、L、M和N) 列的字段类型
                let type=Onefiled_Type_Buffer[0];
                this.Dbfheader_Offset+=4;//保留字节，用于以后添加新的说明性信息时使用，这里用0来填写
                let Onefiled_Lengthe_Buffer=Buffer.alloc(1);
                this.dbf_file_buffer.copy(Onefiled_Lengthe_Buffer, 0, this.Dbfheader_Offset, this.Dbfheader_Offset+1);this.Dbfheader_Offset+=1;//记录项长度，二进制型
                let length=Onefiled_Lengthe_Buffer[0];
                this.Dbfheader_Offset+=1;//记录项的精度，二进制型
                this.Dbfheader_Offset+=2;//保留字节，用于以后添加新的说明性信息时使用，这里用0来填写
                this.Dbfheader_Offset+=1;//工作区ID
                this.Dbfheader_Offset+=10;//保留字节，用于以后添加新的说明性信息时使用，这里用0来填写
                this.Dbfheader_Offset+=1;//MDX标识。如果存在一个MDX 格式的索引文件，那么这个记录项为真，否则为空
                fileds.push({
                    name,
                    type,
                    length,
                });
            }
            this.Dbfheader_Offset+=1;//记录项终止标识
            this.dbf_fileds=fileds;
            return true
        }
        catch{
            return false
        }
        
    }
    Read_dbf_Record_By_buffer(){
        try{
            var values=[];
            for (var i = 0; i < this.dbf_recorde_count;i ++ )
            {
                this.Dbfheader_Offset+=1;//第一个字节是删除标志，若记录被删除，则该字节为0x2A即"*"；否则为0x20即空格
                let onevalue={};
                for (var j = 0; j < this.dbf_fileds.length;j ++ )
                {
                    var name = this.dbf_fileds[j].name;
                    var length = this.dbf_fileds[j].length;
                    var type = this.dbf_fileds[j].type;
                    let Value_Buffer=Buffer.alloc(length);
                    this.dbf_file_buffer.copy(Value_Buffer, 0, this.Dbfheader_Offset, this.Dbfheader_Offset+ length);this.Dbfheader_Offset+=length;//记录项值
                    onevalue[name]=this.dbf_field_convert(this.trim(Value_Buffer),type);
                }
                values.push(onevalue)
            }
            this.dbf_recorde_values=values
            return true
        }catch{
            return false
        }
        
    }
    dbf_field_convert(_valuebytes,_type){
        switch(_type) {
            case ShapeFieldType.SHAPE_B: //Double 。
                return new Number(String.fromCharCode.apply(null, _valuebytes)).valueOf();
            case ShapeFieldType.SHAPE_C: //字符型	各种字符。
                return this.ByteToStr(_valuebytes);
            case ShapeFieldType.SHAPE_D: // 日期型	用于区分年、月、日的数字和一个字符，内部存储按照YYYYMMDD格式。
                return this.ByteToStr(_valuebytes);
            case ShapeFieldType.SHAPE_F: // Float
                return new Number(String.fromCharCode.apply(null, _valuebytes)).valueOf();
            case ShapeFieldType.SHAPE_G: // (General or OLE)	各种字符。
                return String.fromCharCode.apply(null, valuebyte)
            case ShapeFieldType.SHAPE_N: // 数值型(Numeric)	- . 0 1 2 3 4 5 6 7 8 9 
                return new Number( String.fromCharCode.apply(null, _valuebytes)).valueOf();
            case ShapeFieldType.SHAPE_I: // Integer
                return new Number(String.fromCharCode.apply(null, _valuebytes)).valueOf();
            case ShapeFieldType.SHAPE_L: // 逻辑型（Logical）	? Y y N n T t F f (? 表示没有初始
                break;
            case ShapeFieldType.SHAPE_M: // (Memo)	各种字符。
                break;
            case ShapeFieldType.SHAPE_T: //  DateTime
                break;
            case ShapeFieldType.SHAPE_P: // Picture
                break;
            case ShapeFieldType.SHAPE_Y: //货币
                break;
            default:  
                return "";
        }
    }
    trim(_Buffer){
        let valuebyte=[];
        _Buffer.forEach((value)=> {
            if(value!=32 && value!=0){
                valuebyte.push(value);
            }
        })
        return valuebyte
    }
    ByteToStr(_valuebytes){
        let line ="";
        switch(this.LDID) {
            case LDID.UTF_16:
                line = iconv.decode(_valuebytes, 'utf16');
                break;
            case LDID.UTF_8:
                line = iconv.decode(_valuebytes, 'utf8');
                break;
            case LDID.ASNI:
                line = iconv.decode(_valuebytes, 'cp936');
                break;
            default:  
                line = "";   
                break;   
        }
        return line
    }
}
module.exports = Shape