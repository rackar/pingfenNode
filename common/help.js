const fs = require("fs");
const path = require("path");
const config = require("./config");
const mongoose = require("mongoose");
const request = require('request')
Date.prototype.format = function (format, options) {
  var args = {
    "M+": this.getMonth() + 1,
    "d+": this.getDate(),
    "h+": this.getHours(),
    "m+": this.getMinutes(),
    "s+": this.getSeconds(),
    "q+": Math.floor((this.getMonth() + 3) / 3), //quarter
    S: this.getMilliseconds()
  };
  if (/(y+)/.test(format))
    format = format.replace(
      RegExp.$1,
      (this.getFullYear() + "").substr(4 - RegExp.$1.length)
    );
  for (var i in args) {
    var n = args[i];
    if (new RegExp("(" + i + ")").test(format))
      format = format.replace(
        RegExp.$1,
        RegExp.$1.length == 1 ? n : ("00" + n).substr(("" + n).length)
      );
  }
  return format;
}
Date.prototype.format = function (format, options) {
  var args = {
    "M+": this.getMonth() + 1,
    "d+": this.getDate(),
    "h+": this.getHours(),
    "m+": this.getMinutes(),
    "s+": this.getSeconds(),
    "q+": Math.floor((this.getMonth() + 3) / 3), //quarter
    S: this.getMilliseconds()
  };
  if (/(y+)/.test(format))
    format = format.replace(
      RegExp.$1,
      (this.getFullYear() + "").substr(4 - RegExp.$1.length)
    );
  for (var i in args) {
    var n = args[i];
    if (new RegExp("(" + i + ")").test(format))
      format = format.replace(
        RegExp.$1,
        RegExp.$1.length == 1 ? n : ("00" + n).substr(("" + n).length)
      );
  }
  return format;
}
class Help{
  constructor(){
    this.OsType= require("os").type()
    this.RootDir=path.resolve(__dirname, "..") + "/"
    this.ModelNames=[]
  }
  static Https_Options(){
    return {
      key: fs.readFileSync("/etc/nginx/2910531_talatan.com.key"),
      cert: fs.readFileSync("/etc/nginx/2910531_talatan.com.pem")
    }
  }
  static GetIPAdress(){
    let interfaces = require('os').networkInterfaces();
    for(let devName in interfaces){
        var iface = interfaces[devName];
        for(var i=0;i<iface.length;i++){
            var alias = iface[i];
            if(alias.family === 'IPv4' && alias.address !== '127.0.0.1' && !alias.internal){
              return alias.address;
            }
        }
    }
  }
  static Json_Parse(Split) {
    try {
      Split = JSON.parse(Split);
      return Split;
    } catch (e) {
      return null;
    }
  }
  static Get_MongoConnection(testDB,options) {//多个数据库
    mongoose.set('useCreateIndex', true);
    return mongoose.createConnection(testDB,options,(err)=> {
      console.log(testDB,err)
    })
  }
  static Get_MongoId (id) {
    return mongoose.mongo.ObjectId(id);
  }
  static CheckTokenTime(payload) {
    var clockTimestamp = Math.floor(Date.now() / 1000);
    if (clockTimestamp-payload.exp< 0) {
      let newpayload= Object.assign({},payload);delete newpayload.exp;delete newpayload.iat
      return Help.CreateToken(newpayload);
    }
    else{
      return null;
    }
  }
  static Request_Post_Common(_url,data){
    return new Promise((resolve) => {
        try{
            request({
                url: _url,
                method: "POST",
                json: true,
                headers: {
                    "content-type": "application/json",
                    'User-Agent': "Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.1; Trident/4.0; QQWubi 133; SLCC2; .NET CLR 2.0.50727; .NET CLR 3.5.30729; .NET CLR 3.0.30729; Media Center PC 6.0; CIBA; InfoPath.2)",
                },
                form:data,
                timeout: 500,
            }, (error, response, result)=> {
                if(error==null){
                    if(response.statusCode==200){
                        resolve({
                            status:true,
                            result:result,
                        })
                    }
                    else{
                        resolve({
                            status:false,
                            message:result,
                        })
                    }
                }
                else{
                    resolve({
                        status:false,
                        message:error.message,
                    })
                }
            });
        }catch(error){
            resolve({
                status:false,
                message:error.message,
            })
        }
    })
  }
  static Request_Get_Common(_url,data)
  {
    return new Promise((resolve) => {
        try{
            request({
                url: _url,
                method:"get",
                json: true,
                useQuerystring: true,
                headers: {
                    'User-Agent': "Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.1; Trident/4.0; QQWubi 133; SLCC2; .NET CLR 2.0.50727; .NET CLR 3.5.30729; .NET CLR 3.0.30729; Media Center PC 6.0; CIBA; InfoPath.2)",
                },
                encoding:null,
                timeout: 1000,
                qs:data
            },(error,response,result)=>{
                if(error==null){
                    if(response.statusCode==200){
                        resolve({
                            status:true,
                            result:result,
                        })
                    }
                    else{
                        resolve({
                            status:false,
                            message:result,
                        })
                    }
                }
                else{
                    resolve({
                        status:false,
                        message:error.message,
                    })
                }
            })
        }catch(error){
            resolve({
                status:false,
                message:error.message,
            })
        }
    })
  }
  static Get_Pamars(ctx) {
    let pamars={};
    Object.assign(pamars, ctx.request.body);
    Object.assign(pamars, ctx.request.query);
    return pamars;
  }
  static Get_Time_Ticks() {
    return (new Date()).getTime();
  }
  static Get_Page(pagenum=1,pagesize=10,skip=0){
    pagesize=pagesize>0?pagesize-0:10
    pagenum=pagenum>0?pagenum-0:1
    skip=skip>=0?skip-0:0
    let options={
      pagenum,
      pagesize,
    }
    if(pagenum>1){
      skip=(pagenum?(pagenum-1)*pagesize:0)+skip
    }
    if(skip>=0){
      options.skip = skip
    }
    if(pagesize>0){
      options.limit = pagesize
    }
    return options;
  }
  static ctx_back(ctx,_data){
    ctx.response.status = 200;
    ctx.set("Content-Type", "application/json")
    if(_data.status==false){
      ctx.body =Help.Resolve(false,null,_data.message);
    }
    else{
      ctx.body =Help.Resolve(true,_data.result,'');
    }
  }
  static Resolve(ctx,status=true, result=null,message='') {
    if(ctx.res){
      ctx.response.status = 200;
      ctx.set("Content-Type", "application/json")
      ctx.body =Help.Resolve(status,result,message);
    }
    else{
      return {
        status: ctx,
        result: status,
        message:result
      }
    }
  }
  static ConsoleLog(_log){
    console.log(`${new Date().format("yyyy-MM-dd hh:mm:ss")}=>${_log}`)
  }
  static ReadFileBytes(file_saved,size){
    let file_saved_stat=fs.statSync(file_saved);
    if(size && size>0){
      if(file_saved_stat.size!=size){
        return {
          status:false,
          result:'uploads: 上传文件的字节数与参数对应的字节数不一致!'
        }
      }
    }
    let fs_saved=fs.openSync(file_saved,'r');
    let buffer=Buffer.alloc(file_saved_stat.size);
    fs.readSync(fs_saved, buffer, 0, file_saved_stat.size, 0);//读入文件数据到buffer
    fs.closeSync(fs_saved);
    fs.unlinkSync(file_saved);
    return {
      status:true,
      result:buffer
    }
  }
  static WriteFileBytes(_filename,_buffer,_size,_offset){
    let fs_save_true=fs.openSync(_filename,'a');
    fs.writeSync(fs_save_true, _buffer,0,_size,_offset);
    fs.closeSync(fs_save_true);
    return true;
  }
  static Get_Time(filename,withsplit=true){
    let tilenames=filename.split('_')
    if(tilenames.length==7){
      let time=tilenames[3]
      if(withsplit==false){
        return `${time.substring(0,4)}${time.substring(4,6)}${time.substring(6,8)}`
      }
      else{
        return `${time.substring(0,4)}-${time.substring(4,6)}-${time.substring(6,8)}`
      }
    }
    else{
      if(withsplit==false){
        return '00000000'
      }
      else{
        return '0000-00-00'
      }
    }
  }
  static Get_Num_String(i_index=1,string_max_length=6){
    let index_string=i_index+"";
    if(index_string.length<string_max_length){
      let _length=index_string.length
      for (let j = 0; j < string_max_length-_length; j++){
        index_string="0"+index_string;
      }
    }
    return index_string;
  }
  static Del_dir_fn(p){
    let arr=fs.readdirSync(p);
    for(let i in arr){
      let _file=p+'/'+arr[i]//读取文件信息，以便于判断是否是一个文件或目录
      let stats=fs.statSync(_file);
      if(stats.isFile()){//判断为真，是文件则执行删除文件
        fs.unlinkSync(_file);
      }else{
        Help.Del_dir_fn(_file);//判断为假就是文件夹，就调用自己，递归的入口
      }
    }
    fs.rmdirSync(p);//删除空目录
  }
  static Guid(){
    return 'xxxxxxxx_xxxx_4xxx_yxxx_xxxxxxxx'.replace(/[xy]/g, function (c) {
      let r = Math.random() * 16 | 0,v = c == 'x' ? r : (r & 0x3 | 0x8);
      return v.toString(16);
    });
  }
  //JKF01_0075_00038570_20210125111233_0PMS05_003_F.tif
  static Get_JingFile(_filename,server_dir,local_dir){
    if(!server_dir){
      server_dir=config.alibaba_ai.server_dir
    }
    if(!local_dir){
      local_dir=config.alibaba_ai.local_dir 
    }
    let r_filename=''
    let local_filename=''
    let _filename_no_tif=_filename.replace('.tif','')
    let filenames=_filename_no_tif.split('_')
    let bsf=[
      'JKF01','JGF01','JGF02','JGF03','JGF04','JGF05','JGF06','JGF07','JGF08','JGF09','JGF10','JGF11','JGP01','JGP02',
      'GF100','GF1B0','GF1C0','GF1D0','GF200','GF500','GF600','ZY02C','ZY1E0','GF701','ZY301','ZY302','ZY303'
    ]
    let status=true;
    //1 检查是否是标准的分幅
    if(filenames.length==7){
      let bsf_find=''
      let year=''
      let month=''
      let bsf_Index=bsf.findIndex((_bsf)=>{return _bsf==filenames[0]})
      if(bsf_Index<0){
        status=false;
      }
      else{
        bsf_find=bsf[bsf_Index]
      }
      if(filenames[1].length!=4){
        status=false;
      }
      if(filenames[2].length!=8){
        status=false;
      }
      if(filenames[3].length!=14){
        status=false;
      }
      else{
        year=filenames[3].substring(0,4)
        month=filenames[3].substring(4,6)
      }
      if(filenames[4].length!=6){
        status=false;
      }
      if(filenames[5].length!=3){
        status=false;
      }
      if(filenames[6].length!=1){
        status=false;
      }
      //2 如果是则判断文件是否存在
      if(status){
        r_filename=`${server_dir}jing/${year}/${month}/${bsf_find}/${_filename_no_tif}/${_filename_no_tif}.tif`
        local_filename=`${local_dir}jing/${year}/${month}/${bsf_find}/${_filename_no_tif}/${_filename_no_tif}.tif`
        if(!fs.existsSync(local_filename)){
          status=false
        }
        //从S3中查找并下载
        //再次验证
      }
    }
    else{
      status=false;
    }
    if(status){
      return {
        server:r_filename,
        local:local_filename
      }
    }
     //3 否则在temp_data文件夹中寻找
    r_filename=`${server_dir}temp_data/${_filename_no_tif}.tif`
    local_filename=`${local_dir}temp_data/${_filename_no_tif}.tif`
    if(fs.existsSync(local_filename)){
      return {
        server:r_filename,
        local:local_filename
      }
    }
    else{
      return {
        server:null,
        local:null
      }
    }
  }
  static MoveFile(_src,_denst,_delete=false){
    if(!fs.existsSync(_src)){
      return{
        status:false,
        message:`${_src} not find !`
      } 
    }
    let _denst_dir=path.resolve(_denst,"..");
    if (!fs.existsSync(_denst_dir)) {
      fs.mkdirSync(_denst_dir,{recursive:true});
    }
    if(fs.existsSync(_denst)){
      fs.unlinkSync(_denst);
    }
    fs.copyFileSync(_src, _denst)
    if(_delete==true){
      fs.unlinkSync(_src);
    }
    return{
      status:true,
    } 
  }
  static Get_Shpae_Files(_shp_file){
    return {
        shp:_shp_file,
        shx:_shp_file.replace(".shp",".shx"),
        dbf:_shp_file.replace(".shp",".dbf"),
        cpg:_shp_file.replace(".shp",".cpg"),
        prj:_shp_file.replace(".shp",".prj"),
    }
  }
}
module.exports = Help