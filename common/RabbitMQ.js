//https://www.cnblogs.com/wukong-holmes/p/9306733.html
/**
 * 对RabbitMQ的封装
 */
let amqp = require('amqplib');
class RabbitMQ{
    constructor(_url) {
        this.url=_url //连接参数
        this.open = amqp.connect(this.url); //连接队列promise
    }
    DoCallBack(msg,next_msg,cb){
        let data=this.GetMsgData(msg)
        let Nextdata=null
        if(next_msg){
            Nextdata=this.GetMsgData(next_msg)
        }
        if(cb && typeof cb==="function"){
            cb(data,msg,Nextdata);
        }
    }
    GetMsgData(msg){
        if (msg && msg.content) {
            let data=msg.content?msg.content.toString():''
            return data
        }
        return null;
    }
    async connect(){
        try{
            this.conn=await this.open
        }
        catch(error){
            return false
        }
        try{
            this.Channel=await this.conn.createChannel();
            this.Channel.prefetch(1);
        }
        catch(error){
            return false
        }
        

        return true
    }
    async consume(queue,cb){
        if(!this.conn)await this.connect()
        try{
            let assert_options={
                durable: true,
                maxPriority:255
            }
            let AssertQueue=await this.Channel.assertQueue(queue,assert_options)
            
        }
        catch(error){
            return false
        }
        let Consume =await this.Channel.consume(queue,async (msg) => {
            let next_msg=await this.Channel.get(queue,{
                noAck:false,
            })
            if(next_msg){
                this.Channel.nack(next_msg);
            }
            this.DoCallBack(msg,next_msg,cb)
        })
        if(Consume){
            return true
        }
        return false
    }
    async ack(msg,ok=true) {
        if(!this.conn)await this.connect()
        if(ok===false){
            this.Channel.nack(msg)
        }
        else{
            if(msg){
                this.Channel.ack(msg)
            }
        }
        return true
    }
    async send(queue,msg,cb,priority=0){
        try{
            if(!this.conn)await this.connect()
            if(this.Channel){
                let _Buffer=Buffer.from(msg,"utf-8");
                let b_sendToQueue=this.Channel.sendToQueue(queue, _Buffer, {
                    persistent: true,
                    priority:priority,
                    deliveryMode:2,
                });
                if(cb && typeof cb==="function") cb(b_sendToQueue)
                return b_sendToQueue
            }
        }
        catch(e){
            console.log(`sendQueueMsg error:${e.message}`)
            return false 
        }
    }
    async CheckQueue(_queueName) {
        try{
            if(!this.conn)await this.connect()
            let assert_options={
                durable: true,
                maxPriority:255
            }
            await this.Channel.assertQueue(_queueName,assert_options)
        }
        catch(e){
            console.log(`CheckQueue error:${e.message}`)
            return false 
        }
        return true;
    }
    async Close() {
        if(!this.conn)await this.connect()
        this.Channel.close();
    }
}
module.exports = RabbitMQ