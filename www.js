const Koa = require("koa");
const http = require("http");
const WebSocket = require('ws');
const os = require('os');
const json = require("koa-json");
const bodyparser = require("koa-bodyparser");
const jwt = require("koa-jwt");
const config = require("./common/config.js");
const help = require("./common/help.js");
const app = new Koa();
// 解决跨域和options请求问题，集中处理错误
app.use(async (ctx, next) => {
  let origin = ctx.headers.origin;
  //当请求携带cookie时 Access-Control-Allow-Credentials为true时，Access-Control-Allow-Origin不能为*，只能设定origin
  if (origin) {
    ctx.set("Access-Control-Allow-Origin", origin);
  } else {
    ctx.set("Access-Control-Allow-Origin", "*");
  }
  ctx.set("Access-Control-Allow-Methods","POST, GET");//, PATCH, HEAD, PUT, DELETE
  // ctx.set("Access-Control-Max-Age", "3600");
  ctx.set("Access-Control-Allow-Headers","x-requested-with,Authorization,Content-Type,Accept");
  ctx.set("Access-Control-Allow-Credentials", "true");
  //当自定义的Headers希望前端可以自由读取时，需要设置Access-Control-Expose-Headers，否则前端看得到但是读取不到
  ctx.set("Access-Control-Expose-Headers", "Authorization");
  if (ctx.request.method == "OPTIONS") {
    ctx.response.status = 200;
  } 
  else {
    try 
    {
      await next();
    } 
    catch (err) {
      ctx.response.status = err.statusCode || err.status || 500;
      ctx.response.body = {
        message: err.message
      };
    }
  }
});
app.use(bodyparser({
  enableTypes: ["json", "form", "text"],
  encoding:'utf-8', //限制上传的数据量
  formLimit:'50mb',
  jsonLimit:'50mb',
  textLimit:'50mb',
  strict:true,
  detectJSON:null,
  multipart: true,
  extendTypes:{
    // json: ['application/x-javascript'], // will parse application/x-javascript type body as a JSON string
    // form: ['multipart/form-data'], // will parse application/x-javascript type body as a JSON string
  },
  onerror:function (err, ctx) {
    ctx.throw('body parse error', 422);
  }
})
);
app.use(json());
{
  const server = http.createServer(app.callback());
  server.on("error", (error)=>{
    help.ConsoleLog(error.message)
  });
  server.on("listening", ()=>{
    var addr = server.address();
    var bind = "http://" +(addr.address==="::"?help.GetIPAdress():addr.address )+":" + addr.port;
    help.ConsoleLog(`Listening on:${bind}--->进程号：${process.pid}`)
  });
  server.listen(config.port);
  const socket_io_like = require("./common/socket.io.like.js");
  let webSocket_cb=require("./routes/pinfen/webSocket.js").cb
  const wss = new WebSocket.Server({
    server,
    clientTracking:true
  }).on("connection",function(socket,request){//必须要是function(socket,request)的写法才能在下面的方法里面的this获取到clients
    socket.clients=this.clients
    socket_io_like(socket,webSocket_cb)
  });
}
// routes
const upload = require("./routes/upload");
app.use(upload.routes(), upload.allowedMethods());
const pinfen = require("./routes/routes.js");
app.use(pinfen.routes(), pinfen.allowedMethods());
app.use(require("koa-static")(__dirname + "/uploads"));

