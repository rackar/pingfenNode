const router = require("koa-router")();
const pinfenJS = require("./pinfen/route.js");
router.use(pinfenJS.routes(), pinfenJS.allowedMethods());
// router.prefix("/ajax");
module.exports = router;