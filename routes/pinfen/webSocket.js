const PingFenMiddleware=require("./PingFenMiddleware.js")
var my_rest={
  active_socket:null,
  to_active_socket(){

  },
  cb:(socket)=> {
    // socket.groups=[]
    socket.on("add_gamer", async pamars => {
      let result=await PingFenMiddleware.add_gamer(pamars)
      socket._broadcast('get_gamer',result);
    });
    socket.on("delete_gamer", async pamars => {
      let result=await PingFenMiddleware.delete_gamer(pamars)
      socket._broadcast('get_gamer',result);
    });
    socket.on("get_gamer", async pamars => {
      let result=await PingFenMiddleware.get_gamer(pamars)
      socket._emit('get_gamer',result);
    });
    socket.on("update_gamer", async pamars => {
      let result=await PingFenMiddleware.update_gamer(pamars)
      socket._emit('update_gamer',result.result);
      my_rest.cb_active('get_gamer',result.result)
      my_rest.cb_active('show_qianda_status',result.fenshu_status)



      //socket._emit('update_gamer',result);
      // if(my_rest.active_socket){
      //   let get_gamer=await PingFenClss.Get_Gamer()
      //   my_rest.active_socket._emit('get_gamer',get_gamer);
      //   let fenshu=result.result-0
      //   if(fenshu>0){
      //     my_rest.active_socket._emit('show_qianda_status',{
      //       status:3,
      //       fenshu:fenshu
      //     });
      //   }
      //   else{
      //     my_rest.active_socket._emit('show_qianda_status',{
      //       status:2,
      //       fenshu:0
      //     });
      //   }
      // }
    });
    socket.on("get_game", async pamars => {
      let result=await PingFenMiddleware.get_game()
      socket._emit('get_game',result);
    });
    socket.on("update_game", async pamars => {
      let result=await PingFenMiddleware.update_game(pamars)
      socket._broadcast('get_game',result);
    });
    socket.on("get_canshu", async pamars => {
      let result=await PingFenMiddleware.get_canshu()
      socket._emit('get_canshu',result);
    });
    socket.on("update_canshu", async pamars => {
      let result=await PingFenMiddleware.update_canshu(pamars)
      socket._broadcast('get_canshu',result);
    });
    socket.on("set_active_socket", id => {
      my_rest.active_socket=socket;
      socket._emit('set_active_socket',socket.id);
    });
    //控制端
    socket.on("show_page", (index) => {
      my_rest.cb_active('show_page',index)
    });
    socket.on("set_active_cs", (id) => {
      my_rest.cb_active('show_active_cs',id)
    });
    socket.on("show_box", (id) => {
      my_rest.cb_active('show_box',id)
    });
    socket.on("start_time", () => {
      my_rest.cb_active('start_time')
    });
    socket.on("end_time", () => {
      my_rest.cb_active('end_time')
    });
    socket.on("show_time", () => {
      my_rest.cb_active('show_time')
    });
  },
  cb_active(_func,_value){
    if(my_rest.active_socket){
      my_rest.active_socket._emit(_func,_value);
    }
  }
  
};
module.exports =my_rest;