
const model_class = require("./model.js");
const help = require("../../common/help.js");
class PingFen{
    static async Get_Gamer(pamars){
        let conditions={}
        if(pamars){
            let {id} = pamars
            if(id){
                conditions['_id']=help.Get_MongoId(id)
            }
        }
        
        let _Gamer_Model=model_class.Get_Gamer_Model(); 
        let query = await _Gamer_Model.find(conditions);
        return help.Resolve(true,query,'')
    }
    static async Add_Gamer(pamars){
        let {name,password,avatar,describle,showindex} = pamars
        if(!name){
            return help.Resolve(false,null,'需要为name赋值')
        }
        let updateOne={
            name:name,
        }
        if(password){
            updateOne.password=password
        }
        if(avatar){
            updateOne.avatar=avatar
        }
        if(describle){
            updateOne.describle=describle
        }
        let _Gamer_Model=model_class.Get_Gamer_Model(); 
        let count=await _Gamer_Model.countDocuments({
            name:name,
        });
        if(count>0){
            return help.Resolve(false,null,'已经存在该名字的参赛队伍')
        }
        else{
            let result = await _Gamer_Model.updateOne(
                { name:name},
                {
                    $set: updateOne
                },
                { upsert: true }
            );
            return help.Resolve(true,true,'')
        }

    }
    static async Update_Gamer(pamars){
        let {id,name,password,avatar,describle,datifen,qiangda,showindex,datifennum,datifenjindu} = pamars
        if(!id){
            return help.Resolve(false,null,'需要为id赋值')
        }
        let set={}
        if(name){
            set['name']=name
        }
        if(password){
            set['password']=password
        }
        if(avatar){
            set['avatar']=avatar
        }
        if(describle){
            set['describle']=describle
        }
        if(datifen){
            set['datifen']=datifen
        }
        if(showindex){
            set['showindex']=showindex
        }
        if(datifennum){
            set['datifennum']=datifennum
        }
        if(datifenjindu){
            set['datifenjindu']=datifenjindu
        }
        let oid=help.Get_MongoId(id)
        let _Gamer_Model=model_class.Get_Gamer_Model();
        let fenshu=0
        if(qiangda){
            let qiangda_json=qiangda
            if(qiangda_json){
                let finds=await _Gamer_Model.findOne({"_id":oid});
                let qiangdaIns=finds.toJSON().qiangda
                if(!qiangdaIns){
                    qiangdaIns=[]
                }
                let qiangdatotal=0;
                if(Array.isArray(qiangdaIns)){
                    qiangdaIns.forEach((item)=>{
                        qiangdatotal=qiangdatotal+item.fenshu
                    })
                    fenshu=qiangda_json.fenshu-0
                    let in_one={
                        time:new Date().getTime(),
                        num:qiangda_json.num,
                        fenshu:fenshu
                    }
                    set['qiangdatotal']=qiangdatotal+fenshu
                    qiangdaIns.push(in_one)
                    set['qiangda']=qiangdaIns
                }
            }
        }
        await _Gamer_Model.updateOne({"_id":oid},
            {
                $set: set
            },
        );
        if(qiangda){
            return help.Resolve(true,fenshu,'')
        }
        else{
            return help.Resolve(true,0,'')
        }
        
    }
    static async Delete_Gamer(pamars){
        let {id} = pamars
        if(!id){
            return help.Resolve(false,null,'需要为id赋值')
        }
        let oid=help.Get_MongoId(id)
        let _Gamer_Model=model_class.Get_Gamer_Model();
        let _deleteOne = await _Gamer_Model.deleteOne({"_id":oid});
        return help.Resolve(true,true,'')
    }
    static async Get_Game(){
        let _Game_Model=model_class.Get_Game_Model();
        let query = await _Game_Model.find({});
        return help.Resolve(true,query,'')
    }
    static async Update_Game(pamars){
        let {title,zhuban,chenban} = pamars
        let set={}
        if(title){
            set['title']=title
        }
        if(zhuban){
            set['zhuban']=zhuban
        }
        if(chenban){
            set['chenban']=chenban
        }
        let _Game_Model=model_class.Get_Game_Model();
        let finds=await _Game_Model.find({});
        let con={"title":2}
        if(finds.length>0){
            con={'_id':finds[0].toJSON()._id}
        }
        await _Game_Model.updateOne(con,
            {
                $set: set
            },
            { upsert: true }
        );
        return help.Resolve(true,true,'')
    }
    static async Get_Canshu(){
        let _Game_Model=model_class.Get_Canshu_Model();
        let query = await _Game_Model.find({});
        return help.Resolve(true,query,'')
    }
    static async Update_Canshu(pamars){
        let {cunt,time} = pamars
        let set={}
        if(cunt){
            set['cunt']=cunt
        }
        if(time){
            set['time']=time
        }
        let _Game_Model=model_class.Get_Canshu_Model();
        let finds=await _Game_Model.find({});
        let con={"cunt":10}
        if(finds.length>0){
            con={'_id':finds[0].toJSON()._id}
        }
        await _Game_Model.updateOne(con,
            {
                $set: set
            },
            { upsert: true }
        );
        return help.Resolve(true,true,'')
    }
}
module.exports = PingFen;