
const PingFenClss=require("./PingFen.js")
class PingFenMiddleware{
    static async add_gamer(pamars){
        let result=await PingFenClss.Add_Gamer(pamars)
        if(result.status){
          result=await PingFenClss.Get_Gamer()
        }
        return result
    }
    static async delete_gamer(pamars){
        let result=await PingFenClss.Delete_Gamer(pamars)
        if(result.status){
          result=await PingFenClss.Get_Gamer()
        }
        return result
    }
    static async get_gamer(pamars){
        let result=await PingFenClss.Get_Gamer(pamars)
        return result
    }
    static async update_gamer(pamars){
        let result=await PingFenClss.Update_Gamer(pamars)
        let fenshu_status=null
        if(pamars.qiangda){
            fenshu_status={}
            let fenshu=result.result-0
            if(fenshu>0){
                fenshu_status={
                    status:3,
                    fenshu:fenshu
                }
            }
            else{
                fenshu_status={
                    status:2,
                    fenshu:0
                }
            }
        }
        result=await PingFenClss.Get_Gamer()
        return {
            result,
            fenshu_status,
        }
    }
    static async get_game(pamars){
        let result=await PingFenClss.Get_Game()
        return result
    }
    static async update_game(pamars){
        let result=await PingFenClss.Update_Game(pamars)
        result=await PingFenClss.Get_Game()
        return result
    }
    static async get_canshu(pamars){
        let result=await PingFenClss.Get_Canshu()
        return result
    }
    static async update_canshu(pamars){
        let result=await PingFenClss.Update_Canshu(pamars)
        result=await PingFenClss.Get_Canshu()
        return result
    }
}
module.exports = PingFenMiddleware;