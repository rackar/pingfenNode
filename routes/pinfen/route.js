const router = require("koa-router")();
const help = require("../../common/help.js");
const PingFenMiddleware=require("./PingFenMiddleware.js")
const webSocket = require("./webSocket.js");
async function add_gamer(ctx){
    let pamars=help.Get_Pamars(ctx);
    let result=await PingFenMiddleware.add_gamer(pamars)
    webSocket.cb_active('get_gamer',result)
    help.ctx_back(ctx,result)
}
async function delete_gamer(ctx){
    let pamars=help.Get_Pamars(ctx);
    let result=await PingFenMiddleware.delete_gamer(pamars)
    webSocket.cb_active('get_gamer',result)
    help.ctx_back(ctx,result)
}
async function get_gamer(ctx){
    let pamars=help.Get_Pamars(ctx);
    let result=await PingFenMiddleware.get_gamer(pamars)
    help.ctx_back(ctx,result)
}
async function update_gamer(ctx){
    let pamars=help.Get_Pamars(ctx);
    let result=await PingFenMiddleware.update_gamer(pamars)
    webSocket.cb_active('get_gamer',result.result)
    if(result.fenshu_status){
        webSocket.cb_active('show_qianda_status',result.fenshu_status)
    }
    help.ctx_back(ctx,result.result)
}
async function get_game(ctx){
    let result=await PingFenMiddleware.get_game()
    help.ctx_back(ctx,result)
}
async function update_game(ctx){
    let pamars=help.Get_Pamars(ctx);
    let result=await PingFenMiddleware.update_game(pamars)
    webSocket.cb_active('get_game',result)
    help.ctx_back(ctx,result)
}
async function get_canshu(ctx){
    let result=await PingFenMiddleware.get_canshu()
    help.ctx_back(ctx,result)
}
async function update_canshu(ctx){
    let pamars=help.Get_Pamars(ctx);
    let result=await PingFenMiddleware.update_canshu(pamars)
    webSocket.cb_active('get_canshu',result)
    help.ctx_back(ctx,result)
}
//控制端
async function show_page(ctx){
    let pamars=help.Get_Pamars(ctx);
    webSocket.cb_active('show_page',pamars.index-0)
    help.Resolve(ctx,true,true,'')
}
async function set_active_cs(ctx){
    let pamars=help.Get_Pamars(ctx);
    webSocket.cb_active('set_active_cs',pamars.id)
    help.Resolve(ctx,true,true,'')
}
async function show_box(ctx){
    let pamars=help.Get_Pamars(ctx);
    webSocket.cb_active('show_box',pamars.id)
    help.Resolve(ctx,true,true,'')
}
async function start_time(ctx){
    webSocket.cb_active('start_time')
    help.Resolve(ctx,true,true,'')
}
async function end_time(ctx){
    webSocket.cb_active('end_time')
    help.Resolve(ctx,true,true,'')
}
async function show_time(ctx){
    webSocket.cb_active('show_time')
    help.Resolve(ctx,true,true,'')
}
router.post('/add_gamer',add_gamer)
.post('/delete_gamer',delete_gamer)
.get('/get_gamer',get_gamer)
.post('/update_gamer',update_gamer)
.get('/get_game',get_game)
.post('/update_game',update_game)
.get('/get_canshu',get_canshu)
.post('/update_canshu',update_canshu)
.get('/show_page',show_page)
.get('/set_active_cs',set_active_cs)
.get('/show_box',show_box)
.get('/start_time',start_time)
.get('/end_time',end_time)
.get('/show_time',show_time)
// router.prefix("/ajax");
module.exports = router;