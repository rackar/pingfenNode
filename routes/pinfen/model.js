const _MongoConnection = require("./dbconfig");
const Schema = require("mongoose").Schema;
class models{
  static Get_Gamer_Model(){
    let ModelName=`Gamer`
    let model=_MongoConnection.models[ModelName]
    if(model){
      return model
    }
    else{
      return _MongoConnection.model(ModelName, new Schema({
        name: {type: String,default: ''},//单位名称
        password: { type: String,default: '000000' },//单位密码
        avatar: {type: String,default: ''},//单位头像
        describle: {type: String,default: '' },//单位描述
        datifen:{type: Number,default: 0},//答题分
        datifennum:{type: Number,default: 0},//答题题目
        datifenjindu:{type: Number,default: 0},//答题进度
        qiangda:[//抢答分 
          {
            num:{type: String},//抢答题目序号
            time:{type: Number},//抢答时间
            fenshu:{type: Number,default: 0},//抢答分数
          }
        ],
        qiangdatotal:{type: Number,default: 0},//抢答总分
        total:{type: Number,default: 0},//总分
        showindex:{type: Number,default: 1},//显示顺序
      }),ModelName);
    }
  }
  static Get_Canshu_Model(){
    let ModelName=`Canshu`
    let model=_MongoConnection.models[ModelName]
    if(model){
      return model
    }
    else{
      return _MongoConnection.model(ModelName, new Schema({
        cunt: {type: Number},//答题分与抢答分比重
        time: {type: Number,default: 10 },//答题时间
      }),ModelName);
    }
  }
  static Get_Game_Model(){
    let ModelName=`game`
    let model=_MongoConnection.models[ModelName]
    if(model){
      return model
    }
    else{
      return _MongoConnection.model(ModelName, new Schema({
        title: {type: String},
        zhuban: {type: String},
        chenban: {type: String}
      }),ModelName);
    }
  }
}
module.exports = models;