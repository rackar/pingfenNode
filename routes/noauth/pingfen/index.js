const router = require("koa-router")();
var Types = require('mongoose').Types;
const jwt = require("jsonwebtoken");
const config = require("../../../config");
var Pingwei = require("../../../models/pingfen/pingwei");
var webSocket=require("../../../webSocket/index")
const help = require("../../help.js");
async function addPingwei(ctx, next){
    ctx.response.status = 200;
    ctx.set("Content-Type", "application/json")
    let parmars=help.getpamars(ctx);
    //let { username, password } = parmars;
    if (parmars.username === "" || parmars.password === "" || parmars.name === "") {
        ctx.body =help.resolve(false,null,"评委信息不能为空，添加失败");
        return;
    }
    let pw = await Pingwei.findOne({ username: parmars.username });
    if (pw) {
        ctx.body =help.resolve(false,null,"评委用户名已存在，请更换");
        return;
    }
    var regperson = new Pingwei({
        name: parmars.name,
        description: parmars.description,
        username: parmars.username,
        password: parmars.password,
        pwtype: parmars.pwtype,
        avatar: parmars.avatar ? parmars.avatar : "person.png"
    });
    let result = await regperson.save();
    if (result)
    {
        ctx.body =help.resolve(true,{ id: result.id },"新增评委成功");
        //Updated();//更新推送
    }
    else {
        ctx.body =help.resolve(false,null,"新增评委失败");
        return;
    }
}
async function login(ctx, next){
    ctx.response.status = 200;
    ctx.set("Content-Type", "application/json")
    let parmars=help.getpamars(ctx);
    let { username, password } = parmars;
    let result = await Pingwei.findOne({ username, password });
    if (result) {
        let Newresult={
            name: result.name,
            username: result.username,
            _id: result._id
        }
        ctx.body =help.resolve(true,Newresult,"");
    }
    else {
        var reg =/^0?1[3|4|5|6|7|8][0-9]\d{8}$/;
        if(reg.test(username)){
            let regperson = new Pingwei({
                name: username,
                description: '',
                username: username,
                password: password,
                pwtype: '嘉宾', 
                avatar: "person.png"
            });
            await regperson.save();
            let result3 = await Pingwei.findOne({ username, password });
            let Newresult={
                name: result3.name,
                username: result3.username,
                _id: result3._id
            }
            ctx.body =help.resolve(true,Newresult,"");
            //Updated();//更新推送
        }
        else{
            ctx.body =help.resolve(false,null,"用户不是真实的评委或手机号不是正确的11位号码");
        }
    }
}
var cansai = require("../../../models/pingfen/cansai");
async function addcansai(ctx, next){
    ctx.response.status = 200;
    ctx.set("Content-Type", "application/json")
    let parmars=help.getpamars(ctx);
    if (parmars.name === "" || parmars.password === "" || parmars.qjzx === "") {
        ctx.body =help.resolve(false,null,"参赛选手信息不能为空，添加失败");
        return;
    }
    let cs = await cansai.findOne({ name: parmars.name });
    if (cs) {
        ctx.body =help.resolve(false,null,"参赛选手已存在，请更换");
        return;
    }
    var regperson = new cansai({
        name: parmars.name,
        password:parmars.password,
        qjzx:parmars.qjzx,
        avatar: parmars.avatar ? parmars.avatar : "person.png"
    });
    let result = await regperson.save();
    if (result){
        ctx.body =help.resolve(true,{ id: result.id },"新增参赛选手成功");
        //Updated();//更新推送
        return;
    }
    else {
        ctx.body =help.resolve(false,null,"新增参赛选手失败");
        return;
    }
}
async function update_cansai_tkid_datifen(ctx, next){
    ctx.response.status = 200;
    ctx.set("Content-Type", "application/json")
    let parmars=help.getpamars(ctx);
    let { cansaiId, tkid,datifen } = parmars;
    let first = await cansai.findOne({_id:Types.ObjectId(cansaiId)});
    if (first) {
        let result = await cansai.updateOne({_id:Types.ObjectId(cansaiId)},
            {
                $set: {
                    tkid,
                    datifen
                }
            },
        );
        if (result)
        {
            ctx.body =help.resolve(true,result,"更新参赛选手信息的题库序号和分数成功");
            //更新推送
            Updated({
                type:'update_cansai_tkid_datifen',
            });
        }
        else{
            ctx.body =help.resolve(false,null,"更新参赛选手信息的题库序号失败");
        }
      
    } else {
        ctx.body =help.resolve(false,null,"没有找到对应cansaiId的参赛选手");
    }
}
async function update_cansai_shicao_total(ctx, next){
    ctx.response.status = 200;
    ctx.set("Content-Type", "application/json")
    let parmars=help.getpamars(ctx);
    let { cansaiId, shicao,total } = parmars;
    let first = await cansai.findOne({_id:Types.ObjectId(cansaiId)});
    if (first) {
        let result = await cansai.updateOne({_id:Types.ObjectId(cansaiId)},
            {
                $set: {
                    shicao,
                    total
                }
            },
        );
        if (result)
        {
            ctx.body =help.resolve(true,result,"更新参赛选手信息的实操分数成功");
            //更新推送
            Updated({
                type:'update_cansai_shicao_total',
            });
        }
        else{
            ctx.body =help.resolve(false,null,"更新参赛选手信息的实操分数失败");
        }
      
    } else {
        ctx.body =help.resolve(false,null,"没有找到对应cansaiId的参赛选手");
    }
}
async function update_cansai_showindex(ctx, next){
    ctx.response.status = 200;
    ctx.set("Content-Type", "application/json")
    let parmars=help.getpamars(ctx);
    let { cansaiId, showindex } = parmars;
    let first = await cansai.findOne({_id:Types.ObjectId(cansaiId)});
    if (first) {
        let result = await cansai.updateOne({_id:Types.ObjectId(cansaiId)},
            {
                $set: {
                    showindex
                }
            },
        );
        if (result)
        {
            ctx.body =help.resolve(true,result,"更新参赛选手信息的实操分数成功");
           //更新推送
            Updated({
                type:'update_cansai_showindex',
            });
        }
        else{
            ctx.body =help.resolve(false,null,"更新参赛选手信息的实操分数失败");
        }
      
    } else {
        ctx.body =help.resolve(false,null,"没有找到对应cansaiId的参赛选手");
    }
}
async function logincs(ctx, next){
    ctx.response.status = 200;
    ctx.set("Content-Type", "application/json")
    let parmars=help.getpamars(ctx);
    let { name, password } = parmars;
    let result = await cansai.findOne({ name, password });
    if (result) {
        let userData = {
            name: result.name,
            _id: result._id
        };
        ctx.body =help.resolve(true,userData,"参赛选手登录成功");
    } else {
        ctx.body =help.resolve(false,null,"参赛选手登录失败");
    }
}
var huanjie = require("../../../models/pingfen/huanjie");
async function addhuanjie(ctx, next){
    ctx.response.status = 200;
    ctx.set("Content-Type", "application/json")
    let parmars=help.getpamars(ctx);
    var regperson = new huanjie({
        name: parmars.name
    });
    let result = await regperson.save();
    if (result){
        ctx.body =help.resolve(true,{ id: result.id },"新增环节成功");
        //更新推送
        Updated({
            type:'addhuanjie',
        });
    }
    else {
        ctx.body =help.resolve(false,null,"新增环节失败");
    }
}
var flow = require("../../../models/pingfen/flow");
var ObjectID = require("mongodb").ObjectID;
async function changeHuanjie(ctx, next){
    ctx.response.status = 200;
    ctx.set("Content-Type", "application/json")
    let parmars=help.getpamars(ctx);
    let { cansaiId, huanjieId } = parmars;
    let first = await flow.findOne({});
    let _id = new ObjectID();
    if (first){
        _id = first._id;
    }
    let result = await flow.updateOne({ _id: _id },
        {
            $set: {
                cansaiId: cansaiId,
                huanjieId: huanjieId
            }
        },
        { upsert: true }
    );
    if (result){
        ctx.body =help.resolve(true,result,"环节变更成功");
        //更新推送
        Updated({
            type:'changeHuanjie',
        });
    } 
    else {
        ctx.body =help.resolve(false,null,"环节变更失败");
    }
}
var Bisai = require("../../../models/pingfen/bisai");
async function changebisai(ctx, next){
    ctx.response.status = 200;
    ctx.set("Content-Type", "application/json")
    let parmars=help.getpamars(ctx);
    let { zhuban, title,chenban } = parmars;
    let first = await Bisai.findOne({});
    let _id = new ObjectID();
    if (first){
        _id = first._id;
    }
    let result = await Bisai.updateOne({ _id: _id },
        {
            $set: {
                zhuban: zhuban,
                title: title,
                chenban: chenban
            }
        },
        { upsert: true }
    );
    if (result){
        ctx.body =help.resolve(true,result,"比赛变更成功");
        //更新推送
        Updated({
            type:'changebisai',
        });
    }
    else {
        ctx.body =help.resolve(false,null,"比赛变更失败");
    }
}
var Canshu = require("../../../models/pingfen/canshu");
async function updatecanshu(ctx, next){
    ctx.response.status = 200;
    ctx.set("Content-Type", "application/json")
    let parmars=help.getpamars(ctx);
    let { datime, pw_cout,jb_cout,pingfen_Cont ,dati_Cont  } = parmars;
    let first = await Bisai.findOne({});
    let _id = new ObjectID();
    if (first){
        _id = first._id;
    }
    let result = await Canshu.updateOne({ _id: _id },
        {
            $set: {
                datime, pw_cout,jb_cout,pingfen_Cont ,dati_Cont
            }
        },
        { upsert: true }
    );
    if (result){
        ctx.body =help.resolve(true,result,"比赛参数变更成功");
        //更新推送
        Updated({
            type:'updatecanshu',
        });
    }
    else {
        ctx.body =help.resolve(false,null,"比赛参数变更失败");
    }
}
var Record = require("../../../models/pingfen/record");
async function getAll(ctx, next){
    ctx.response.status = 200;
    ctx.set("Content-Type", "application/json")
    let pamars=help.getpamars(ctx)
    let {type,pingweiId,cansaiId}=pamars;
    let cs =[];let hj =[];let pw =[];let flowa =[];let bisai =[];let canshu =[];let record =[];
    if(type){
        let typeAll=type.split(',')
        if(typeAll && Array.isArray(typeAll)){
            for (let index = 0; index < typeAll.length; index++) {
                let element_type = typeAll[index];
                if(element_type=='cs'){
                    cs = await cansai.find().sort({showindex:1})
                }
                else if(element_type=='hj'){
                    hj = await huanjie.find();
                }
                else if(element_type=='pw'){
                    pw = await Pingwei.find();
                }
                else if(element_type=='flow'){
                    flowa = await flow.find();
                }
                else if(element_type=='bisai'){
                    bisai = await Bisai.find();
                }
                else if(element_type=='canshu'){
                    canshu = await Canshu.find();
                }
                else if(element_type=='record'){
                    let q_record={};
                    if(pingweiId){
                        q_record.pingweiId=pingweiId
                    }
                    if(cansaiId){
                        q_record.cansaiId=cansaiId
                    }
                    record = await Record.find(q_record);
                }
            }
        }
    }
    
    ctx.body =help.resolve(true,{ cs, hj, pw, flow:flowa, record, bisai,canshu },"获取信息成功");
    
}
async function getAll2(ctx, next){
    ctx.response.status = 200;
    ctx.set("Content-Type", "application/json")
    let pamars=help.getpamars(ctx)
    let {type,pingweiId,cansaiId}=pamars;
    let cs =[];let hj =[];let pw =[];let flowa =[];let bisai =[];let canshu =[];let record =[];
    if(type){
        let typeAll=type.split(',')
        if(typeAll && Array.isArray(typeAll)){
            let 
            for (let index = 0; index < typeAll.length; index++) {
                let element_type = typeAll[index];
                if(element_type=='cs'){
                    cs = await cansai.find().sort({showindex:1})
                    // let ccc=cansai.find().sort({showindex:1})
                    // console.log(ccc);
                    // ccc.then((data)=>{
                    //     console.log(data)
                    // })
                    //cs = await ccc
                    
                }
                else if(element_type=='hj'){
                    hj = await huanjie.find();
                }
                else if(element_type=='pw'){
                    pw = await Pingwei.find();
                }
                else if(element_type=='flow'){
                    flowa = await flow.find();
                }
                else if(element_type=='bisai'){
                    bisai = await Bisai.find();
                }
                else if(element_type=='canshu'){
                    canshu = await Canshu.find();
                }
                else if(element_type=='record'){
                    let q_record={};
                    if(pingweiId){
                        q_record.pingweiId=pingweiId
                    }
                    if(cansaiId){
                        q_record.cansaiId=cansaiId
                    }
                    record = await Record.find(q_record);
                }
            }
        }
    }
    
    ctx.body =help.resolve(true,{ cs, hj, pw, flow:flowa, record, bisai,canshu },"获取信息成功");
    
}
async function del(ctx, next){
    ctx.response.status = 200;
    ctx.set("Content-Type", "application/json")
    let parmars=help.getpamars(ctx);
    let result = null;
    if (parmars.type == "pw") {
        result = await Pingwei.remove({_id: ObjectID(parmars._id)});
    } else if (parmars.type == "cs") {
        result = await cansai.remove({_id: ObjectID(parmars._id)});
    } else if (parmars.type == "hj") {
        result = await huanjie.remove({_id: ObjectID(parmars._id)});
    } else {
        ctx.body =help.resolve(false,null,"type 参数只支持pw cs hj");
        return;
    }
    if (result) {
        ctx.body =help.resolve(true,result,"删除成功");
        //更新推送
        Updated({
            type:'del',
            data:'pw,cs,hj'
        });
    }
    else{
        ctx.body =help.resolve(false,null,"删除失败");
    }
}
async function add(ctx, next){
    ctx.response.status = 200;
    ctx.set("Content-Type", "application/json")
    let parmars=help.getpamars(ctx);
    let { cansaiId, huanjieId, pingweiId, fenshu } = parmars;
    let first = await flow.findOne({});
    if (first) {
        if (cansaiId == first.cansaiId && huanjieId == first.huanjieId) {
            let addData={
                cansaiId,
                huanjieId,
                pingweiId,
                fenshu
            }
            let result = await Record.updateOne({ cansaiId, huanjieId, pingweiId },
                {
                    $set: addData
                },
                { upsert: true }
            );
            if (result){
                ctx.body =help.resolve(true,result,"评分成功");
                //更新推送
                Updated({
                    type:'add',
                    data:addData
                });
            }
            else{
                ctx.body =help.resolve(false,null,"评分失败");
            }
        }
        else {
            ctx.body =help.resolve(false,null,"未开始评分");
        }
    } else {
        ctx.body =help.resolve(false,null,"获取环节信息失败");
    }
}
async function dafenpost(ctx, next){
    ctx.response.status = 200;
    ctx.set("Content-Type", "application/json")
    let parmars=help.getpamars(ctx);
    let { cansaiId, fenshu } = parmars;//cansaiId 为tkid 写错了。。但是对方已经是这样了 所以就这样吧。。。
    if(cansaiId && fenshu>=0){
        let result = await cansai.updateOne({tkid:cansaiId},{
            $set: {
                datifen:fenshu-0,
            }
        });
        if (result && result.n==1 && result.nModified==1 && result.ok==1)  
        {
            ctx.body =help.resolve(true,result,"上传答题分成功");
            //更新推送
            Updated({
                type:'dafenpost'
            });
        } else {
            ctx.body =help.resolve(false,result,"上传答题分失败 可能是没有找到对应的题库号（cansaiId）");
        }
    }
    else{
        ctx.body =help.resolve(false,null,"cansaiId fenshu 参数必须设置正确");
    }
}
function Updated(value){
    if(webSocket.avtive ){ 
        webSocket.avtive.socket.emit("updated",value);
    }
}
router.prefix("/pingfen");
router.post("/all", getAll); //查询所有数据
router.post("/addpingwei", addPingwei); //新增评委
router.post("/addcansai", addcansai); //新增队伍
router.post("/addhuanjie", addhuanjie); //新增比赛环节
router.post("/changehuanjie", changeHuanjie); //设定当前为哪个环节和队伍评分
router.post("/changebisai", changebisai); //设定当前为哪个环节和队伍评分
router.post("/update_cansai_tkid_datifen", update_cansai_tkid_datifen);//更新队伍题库和分数
router.post("/update_cansai_shicao_total", update_cansai_shicao_total); //更新队伍实操和总分
router.post("/update_cansai_showindex", update_cansai_showindex); //更新队伍出场顺序
router.post("/updatecanshu", updatecanshu); //更新参数
router.post("/login", login); //评委登录
router.post("/cslogin", logincs); //参赛登录
router.post("/delete", del); //删除某一条数据
router.post("/add", add); //评委打分
router.post("/dafenpost", dafenpost); //推送评分
module.exports = router;