const router = require("koa-router")();
const multer = require("koa-multer");
router.prefix("/upload");
const storage = multer.diskStorage({
  destination: function(req, file, cb) {
    cb(null, "uploads/");
  },
  filename: function(req, file, cb) {
    cb(null, file.fieldname + "-" + Date.now() + file.originalname);
  }
});
const upload = multer({ storage: storage });
router.post("/image", upload.single("file"), function(ctx, next) {
  ctx.body = {
    status: true,
    message: "上传图片成功",
    path: ctx.req.file.path,
    filename: ctx.req.file.filename,
    contentType: ctx.req.file.mimetype
  };
});
module.exports = router;
