const Koa = require("koa");
const app = new Koa();
const json = require("koa-json");
const bodyparser = require("koa-bodyparser");
const noauth = require("./routes/noAuth");

const jwt = require("koa-jwt");
const config = require("./config/index");
// 解决跨域和options请求问题，集中处理错误
const handler = async (ctx, next) => {
  ctx.set("Access-Control-Allow-Origin", "*");
  ctx.set(
    "Access-Control-Allow-Methods",
    "POST, GET, OPTIONS, PATCH, HEAD, PUT, DELETE"
  );
  ctx.set("Access-Control-Max-Age", "3600");
  ctx.set(
    "Access-Control-Allow-Headers",
    "x-requested-with,Authorization,Content-Type,Accept"
  );
  ctx.set("Access-Control-Allow-Credentials", "true");
  if (ctx.request.method == "OPTIONS") {
    ctx.response.status = 200;
  } 
  else {
    try {
      await next();
    } catch (err) {
      ctx.response.status = err.statusCode || err.status || 500;
      ctx.response.body = {
        message: err.message
      };
    }
  }
};
// middlewares
app.use(
  bodyparser({
    enableTypes: ["json", "form", "text"]
  })
);
app.use(json());
// app.use(logger());
app.use(require("koa-static")(__dirname + "/uploads"));
app.use(handler);
// logger
app.use(async (ctx, next) => {
  const start = new Date();
  await next();
  const ms = new Date() - start;
  console.log(`${ctx.headers.origin} ${ctx.method} ${ctx.url} - ${ms}ms`);
});
app.use(jwt({ secret: config.jwtsecret }).unless({path: [/^\/noauth/]}));
// routes
app.use(noauth.routes(), noauth.allowedMethods());
//上传文件
const api = require("./routes/api");
app.use(api.routes(), api.allowedMethods());
module.exports = app;
