var mongoose = require("../../api/db_mongoose");
var Schema = mongoose.Schema;
var RecordSchema = new Schema(
  {
    huanjieId: String,
    cansaiId: String,
    pingweiId: String,
    fenshu: Number
  },
  { timestamps: true }
);
module.exports = mongoose.model("Record", RecordSchema);
