var mongoose = require("../../api/db_mongoose");
var Schema = mongoose.Schema;
var BisaiSchema = new Schema(
  {
    title: {
      type: String
    },
    zhuban: {
      type: String
    },
    chenban: {
      type: String
    }
  },
  { timestamps: true }
);
module.exports = mongoose.model("Bisai", BisaiSchema);
