var mongoose = require("../../api/db_mongoose");
var Schema = mongoose.Schema;
var canshu = new Schema(
  {
    datime: Number,//答题时间
    pw_cout: Number,//评委的分比重
    jb_cout: Number,//嘉宾的分比重
    pingfen_Cont: Number,//实操作的分比重
    dati_Cont: Number,//答题的分比重
  },
  { timestamps: true }
);
module.exports = mongoose.model("canshu", canshu);
