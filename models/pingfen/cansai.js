var mongoose = require("../../api/db_mongoose");
var Schema = mongoose.Schema;
var CansaiSchema = new Schema(
  {
    name: {type: String},
    password: { type: String },
    avatar: {type: String},
    qjzx: {type: String},
    tkid:{type: Number},
    datifen:{type: Number},//答题分
    shicao:{type: Number},//实操分
    total:{type: Number},//总分
    showindex:{type: Number},//显示顺序
  },
  { timestamps: true }
);
module.exports = mongoose.model("Cansai", CansaiSchema);
