var X={
  cb:(socket)=> {
    X.sockets.push({
      id: socket.id,
      socket: socket
    })
    socket.on("pingfen", data => {
      console.log("评委提交的评分为：", data);
      socket.broadcast.emit("pingfenResult", data);
    });
    socket.on("huanjie", data => {
      console.log("环节修改为：", data);
      socket.broadcast.emit("huanjieChange", data);
    });
    socket.on("setsocket", data => {
      //另外的简单写法
      let findIndex=X.sockets.findIndex((value, index, arr) => {
        return value.id==data
      })
      if(findIndex>=0){
        X.avtive=X.sockets[findIndex]
      }
    })
    socket.on("disconnect",(data)=>{
      let findIndex=X.sockets.findIndex((value, index, arr) => {
        return value.id==socket.id
      })
      if(findIndex>=0){
        if(X.avtive && (X.avtive.id==X.sockets[findIndex].id)){
          X.avtive=null
        }
        X.sockets.splice(findIndex,1)
      }
    });
  },
  sockets:[],
  avtive:null,
};
module.exports =X ;
